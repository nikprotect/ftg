from .. import loader, utils, main
from asyncio import sleep
from telethon.tl import functions
import requests
try:
	from fake_useragent import UserAgent
except:
	import os
	os.system("pip3 install fake_useragent")
@loader.tds
class LeakMod(loader.Module):
	"""Поиск треков и альбомов по UPC и ISRC"""
	strings = {'name': 'TrackSearch by nixyrii14'}
	def __init__(self):
		self.upc = ''
		self.isrc = ''
		self.headers = {
			'User-Agent': UserAgent().chrome,
			'cookie': '_fbp=fb.1.1637504282820.1531842069', 
			'cookie': 'f_user=606bb1d23400008d8ca53823'
		}
	async def upccmd(self, msg):
		"""Поиск по UPC"""
		try:
			self.upc = msg.message.split('upc ')[1]
		except:
			await msg.edit("Error.")
		else:
			await msg.edit("<b>Поиск...</b>")
			self.url = f"https://console-api.feature.fm/smartlink-resolver/?q=upc:{self.upc}"
			self.r = requests.get(self.url, headers=self.headers).content
			await msg.client.send_message(msg.to_id, str(r))
			await msg.delete()
	async def isrccmd(self, msg):
		await msg.edit("<b>Поиск...</b>")
		# Код для работы с ISRC
		await msg.delete()
